# aws-cicd-pipeline-github-to-lightsail

AWS Lightsail + AWS IAM + AWS CodeDeploy + AWS CodePipeline + AWS CodeBuild

# Steps

* setup service role

using terraform: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_service_linked_role

* install lightsail agent on lightsail instance

* create CodeDeploy application

create codedeploy service role

* create LightSailCodeDeployUser 

arn:aws:iam::371584219818:user/LightSailCodeDeployUser
access key: AKIAVNBBFHKVK7N4FAWC
secret key: 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

* register codedeploy to lightsail instance

```bash
aws deploy register-on-premises-instance --instance-name codedeploy --iam-user-arn <IAM User ARN> --region <Desired Region>
```

to verify the codedeploy is successfully registered

```bash
aws deploy list-on-premises-instances --region <Desired Region>
```

* tag the lightsail instance

```bash
aws deploy add-tags-to-on-premises-instances --instance-names codedeploy --tags Key=Name,Value=CodeDeployLightsailDemo --region <Desired Region>
```


# Automation

tbd.



# Reference

* [Using AWS CodeDeploy and AWS CodePipeline to Deploy Applications to Amazon Lightsail](https://aws.amazon.com/blogs/compute/using-aws-codedeploy-and-aws-codepipeline-to-deploy-applications-to-amazon-lightsail/)
* [Streamline Your Software Release Process Using AWS CodePipeline](https://www.youtube.com/watch?v=zMa5gTLrzmQ)
* [What is AWS CodeBuild?](https://docs.aws.amazon.com/codebuild/latest/userguide/welcome.html)
